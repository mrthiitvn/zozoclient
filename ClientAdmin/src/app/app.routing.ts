import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewRouteComponent } from './components/share/view.route.compnent'
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UserComponent } from './components/user/user.component';
import { UserDetailComponent } from './components/user/user.detail.component';
import { OrderComponent } from './components/order/order.component';
import { ShopComponent } from './components/shop/shop.component';
import { LoginComponent } from './components/login/login.component';
import { ConfigTypeComponent } from './components/config-type/config-type.component';
import { ConfigTypeDetailComponent } from './components/config-type/config-type.detail.component';
import { ConfigComponent } from './components/config/config.component';
import { ConfigDetailComponent } from './components/config/config.detail.component';
import { PhotoTypeComponent } from './components/photo-type/photo-type.component';
import { PhotoTypeDetailComponent } from './components/photo-type/photo-type.detail.component';
import { RoleComponent } from './components/role/role.component'


const frontPageRoutes: Routes = [
]

const adminRoutes: Routes = [
    // { path: '', component: DashboardComponent},
    // { path: 'user', component: UserComponent},
    // { path: 'user/edit/:id', component: UserDetailComponent},
    // { path: 'order', component: OrderComponent},
    // { path: 'shop', component: ShopComponent}
]

const appRoutes: Routes = [
    { path: '', redirectTo: '/', pathMatch: 'full' },
    // { path: '', component: DashboardComponent, children: adminRoutes},
    { path: 'login', component: LoginComponent},
    { path: 'dashboard', component: DashboardComponent},
    { path: 'user', component: UserComponent},
    { path: 'user/edit/:id', component: UserDetailComponent},
    { path: 'shop', component: ShopComponent},
    { path: 'order', component: OrderComponent},
    { path: 'configtype', component: ConfigTypeComponent },
    { path: 'configtype/edit/:id', component: ConfigTypeDetailComponent },
    { path: 'config', component: ConfigComponent },
    { path: 'config/edit/:id', component: ConfigDetailComponent },
    { path: 'phototype', component: PhotoTypeComponent },
    { path: 'phototype/edit/:id', component: PhotoTypeDetailComponent },
    { path: 'role', component: RoleComponent},


];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

