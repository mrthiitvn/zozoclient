import { Component, ViewChild, OnInit, ViewContainerRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { SizeService } from '../../services/size.service';
//Import ToastsManager
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'app-size',
  templateUrl: './size.component.html',
  styleUrls: ['./size.component.css']
})
export class SizeComponent implements OnInit {

  public p: number = 1;
  public objectList: any = [];
  public totalItem: number;
  public showItem: number = 10;

  //sorting
  public key: string = 'Name'; //set default
  public reverse: boolean = false;

  public selectedItem: any;
  @ViewChild('deleteObjectModal') public deleteObjectModal: ModalDirective;
  @ViewChild('changeObjectModal') public changeObjectModal: ModalDirective;
  constructor(public configTypeService: SizeService, public toastr: ToastsManager, vcr: ViewContainerRef) {
     this.toastr.setRootViewContainerRef(vcr);
   }

  ngOnInit() {
    this.getObject();
  }

  getObject(): void {
    this.configTypeService.getSize().then(result => {
      if (result != null && result.success) {
        this.objectList = result.data;
        this.totalItem = this.objectList.length;
      }
    }).catch(function(error){
      console.log(error);
    })
  };

  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }

  confirmDeleteObject(item): void {
    this.selectedItem = item;
    this.deleteObjectModal.show();
  };


  deleteObject(): void {
    this.configTypeService.deleteSize(this.selectedItem).then(result => {
      if(result != null && result.success) {
        this.getObject();
        this.toastr.success('Xóa size thành công!', 'Thông báo!');
      }
      else{
        this.getObject();
        this.toastr.error('Có lỗi xảy ra!', 'Rất tiếc!');
      }
    }).catch(function(error){
      console.log(error);
    })
  };

  confirmChangeObject(item): void {
    this.selectedItem = item;
    this.changeObjectModal.show();
  };


  changeObject(): void {
    this.configTypeService.changeSize(this.selectedItem).then(result => {
      if(result != null && result.success) {
        this.getObject();
        this.toastr.success('Đổi trạng thái size thành công!', 'Thông báo!');
      }
      else{
        this.getObject();
        this.toastr.error('Có lỗi xảy ra!', 'Rất tiếc!');
      }
    }).catch(function(error){
      console.log(error);
    })
  };

}
