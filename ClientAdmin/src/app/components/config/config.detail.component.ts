import { Component, ViewChild, OnInit, ViewContainerRef } from '@angular/core';
import { ConfigService } from '../../services/config.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
//Import ToastsManager
import { ToastsManager } from 'ng2-toastr/ng2-toastr';


@Component({
    selector: 'detail-config',
    templateUrl: './config.detail.component.html'
})

export class ConfigDetailComponent implements OnInit {
    private Id: number = 0;
    public model: any = {
        Id: 0,
        Name: '',
        Value: '',
        ConfigTypeId: null
    };

    private configTypes: any = [];

    constructor(private router: Router, private config: ConfigService, private route: ActivatedRoute, public toastr: ToastsManager, vcr: ViewContainerRef) {
        this.toastr.setRootViewContainerRef(vcr); 
        this.route.params.subscribe(params => {
            this.Id = +params['id']; // (+) converts string 'id' to a number
     
            // In a real app: dispatch action to load the details here.
         });
    };

    ngOnInit() {
        this.getConfigType();
        if (this.Id != 0) {
            this.GetObjectById();
            
        }
    };

    getConfigType(): void {
        this.config.getConfigType()
        .then(result => {
            if (result != null && result.success) {
                this.configTypes = result.data;
            }
        })
        .catch(function(error) {
            console.log(error);
        })
    };


    GetObjectById(): void {
        this.config.getConfigById(this.Id)
        .then(result => {
            if (result != null && result.success) {
                this.model = result.data;
            }
            else {
                this.toastr.error('Có lỗi xảy ra!', 'Rất tiếc!');
            }
        })
        .catch(function(error) {
            console.log(error);
        })
    };


    Save(type): void {
        if (this.Id == 0) {
            this.config.createConfig(this.model)
            .then(result => {
                if (result != null && result.success) {
                    this.router.navigate(["/config"]);
                    this.toastr.success("Thêm cấu hình thành công!!!", "Thông báo");                    
                }
                else {
                    this.toastr.error("Có lỗi xảy ra!!!", "Rất tiếc");
                }
            })
            .catch(function(error) {
                console.log(error);
            })
        }
        else {
            this.config.updateConfig(this.model).then(result => {
                if (result != null && result.success) {
                    if (type == 0) {
                        this.router.navigate(["/config"]);
                        this.toastr.success("Cập nhật cấu hình thành công!!!", "Thông báo");
                    }
                    else {                    
                        this.toastr.success("Cập nhật cấu hình thành công!!!", "Thông báo");
                    }
                }
                else {
                    this.toastr.error("Có lỗi xảy ra!!!", "Rất tiếc");
                }
            }).catch(function(error) {
                console.log(error);
            })        
        }
    };

    setNewUser(data): void {
        this.model.ConfigTypeId = data;
        //console.log(user);
        //this.curUser = user;
    }
}