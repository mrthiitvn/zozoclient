import { Component, ViewChild, OnInit, ViewContainerRef } from '@angular/core';
import { ConfigTypeService } from '../../services/config.type.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
//Import ToastsManager
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'detail-configtype',
  templateUrl: './config-type.detail.component.html'
})
export class ConfigTypeDetailComponent implements OnInit {
  private Id: number = 0;
  public model: any = {
    Id: 0,
    Name: ''
  }
 
  constructor(private router: Router, private configType: ConfigTypeService, private route: ActivatedRoute, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr); 
    this.route.params.subscribe(params => {
        this.Id = +params['id']; // (+) converts string 'id' to a number
 
        // In a real app: dispatch action to load the details here.
     });
   }

  ngOnInit() {
    if (this.Id != 0) {
        this.GetObjectById();
    }
    
  }

  GetObjectById(): void {
    this.configType.getConfigTypeById(this.Id).then(result => {
        if(result != null && result.success){
            this.model = result.data;
        }
        else {
            this.toastr.error('Có lỗi xảy ra!', 'Rất tiếc!');
        }
    }).catch(function(error) {
        console.log(error);
    })
  }
  Save(type): void {
    if(this.Id == 0) {
        this.configType.createConfigType(this.model).then(result => {
            if (result != null && result.success) {
                this.router.navigate(["/configtype"]);
                this.toastr.success("Thêm loại cấu hình thành công!!!", "Thông báo");
            }
            else {
                this.toastr.error("Có lỗi xảy ra!!!", "Rất tiếc");
            }
        }).catch(function(error) {
            console.log(error);
        })
    }
    else {
        this.configType.updateConfigType(this.model).then(result => {
            if (result != null && result.success) {
                if (type == 0) {
                    this.router.navigate(["/configtype"]);
                    this.toastr.success("Cập nhật loại cấu hình thành công!!!", "Thông báo");
                }
                else {                    
                    this.toastr.success("Cập nhật loại cấu hình thành công!!!", "Thông báo");
                }
            }
            else {
                this.toastr.error("Có lỗi xảy ra!!!", "Rất tiếc");
            }
        }).catch(function(error) {
            console.log(error);
        })
    }
  }
}
