import { Component, OnInit, ViewChild } from '@angular/core';
import { RoleService } from '../../services/role.service';
import { ModalDirective } from 'ngx-bootstrap';
import swal from 'sweetalert2'

@Component({
  selector: 'app-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.css']
})
export class RoleComponent implements OnInit {
  public roleList: any;
  public totalItem: number;
  public selectedRole: any = {
    Id: null,
    RoleName: '',
    Status: 1
  };
  @ViewChild('RoleModal') public RoleModal: ModalDirective;
  constructor(private roleService: RoleService) { }

  ngOnInit() {
    this.GetAll();
  }

  GetAll(): void {
    this.roleService.GetAll().then(result => {
      if(result != null && result.data != null && result.data.Result != null){
        this.roleList = result.data.Result;
        this.totalItem = result.data.Result.length;
      }
      else{
        swal('','get all user error', 'error');
      }
    }).catch(function(error){
      swal('',error, 'error');
    });
  }

  OpenRoleModal(role): void {
    this.RoleModal.show();
  }
}
