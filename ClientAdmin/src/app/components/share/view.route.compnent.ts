import { Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Component(
    {
        selector: 'view-route',
        template: `<router-outlet></router-outlet>`,
        styleUrls: ['view.route.component.css'],
    }
)
export class ViewRouteComponent {
}