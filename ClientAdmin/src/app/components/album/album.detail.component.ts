import { Component, ViewChild, OnInit, ViewContainerRef } from '@angular/core';
import { AlbumService } from '../../services/album.service';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
//Import ToastsManager
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
  selector: 'detail-album',
  templateUrl: './album.detail.component.html'
})
export class AlbumDetailComponent implements OnInit {
  private Id: number = 0;
  public model: any = {
    Id: 0,
    Name: ''
  }
 
  constructor(private router: Router, private album: AlbumService, private route: ActivatedRoute, public toastr: ToastsManager, vcr: ViewContainerRef) {
    this.toastr.setRootViewContainerRef(vcr); 
    this.route.params.subscribe(params => {
        this.Id = +params['id']; // (+) converts string 'id' to a number
 
        // In a real app: dispatch action to load the details here.
     });
   }

  ngOnInit() {
    if (this.Id != 0) {
        this.GetObjectById();
    }
    
  }

  GetObjectById(): void {
    this.album.getAlbumById(this.Id).then(result => {
        if(result != null && result.success){
            this.model = result.data;
        }
        else {
            this.toastr.error('Có lỗi xảy ra!', 'Rất tiếc!');
        }
    }).catch(function(error) {
        console.log(error);
    })
  }
  Save(type): void {
    if(this.Id == 0) {
        this.album.createAlbum(this.model).then(result => {
            if (result != null && result.success) {
                this.router.navigate(["/album"]);
                this.toastr.success("Thêm album thành công!!!", "Thông báo");
            }
            else {
                this.toastr.error("Có lỗi xảy ra!!!", "Rất tiếc");
            }
        }).catch(function(error) {
            console.log(error);
        })
    }
    else {
        this.album.updateAlbum(this.model).then(result => {
            if (result != null && result.success) {
                if (type == 0) {
                    this.router.navigate(["/album"]);
                    this.toastr.success("Cập nhật album thành công!!!", "Thông báo");
                }
                else {                    
                    this.toastr.success("Cập nhật album thành công!!!", "Thông báo");
                }
            }
            else {
                this.toastr.error("Có lỗi xảy ra!!!", "Rất tiếc");
            }
        }).catch(function(error) {
            console.log(error);
        })
    }
  }
}
