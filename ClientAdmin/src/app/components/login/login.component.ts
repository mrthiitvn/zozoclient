import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SessionData } from '../../model/session.data.model';
import { LoginService } from '../../services/login.service';
import { AuthenticateService } from '../../services/authenticate.service';
import { SocketService } from '../../services/socket.service';
import swal from 'sweetalert2'
import { Utils } from '../../utils/utils'

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public sessionData: SessionData;
  public modelLogin: any = {
    userName: '',
    passWord: ''
  }
  public userName = '';
  public passWord = '';
  public socket: any;
  public token: string;
  constructor(
    private loginService: LoginService,
    private authenticateService: AuthenticateService,
    private socketService: SocketService,
    private router: Router,
    private utils: Utils
  ) {
    this.socketService.socket.subscribe(o => {
      this.sessionData = o;
    });
  }

  ngOnInit() {
  }

  Login(): void {debugger;
    this.userName = this.modelLogin.userName;
    this.passWord = this.modelLogin.passWord;
    this.loginService.Login(this.userName, this.passWord).then(result => {
      if (result != null && result.success && result.data != null) {
        var data = JSON.parse(result.data);
        var userId = data.id;
        this.token = data.auth_token;
        var sessionData = this.authenticateService.buildTokenSessionData(this.token);
        this.authenticateService.setSessionData(sessionData);
        this.loginService.GetMe(userId).then(userData => {
          console.log("userData", userData);
          var sessionUserData = this.authenticateService.buildSessionData(userData);
          sessionUserData.Token = this.token;
          this.authenticateService.setSessionData(sessionUserData);
          if (this.socket) {
            this.socket.emit('authenticate', { Token: this.sessionData.Token });
          }
          swal('','Đăng nhập thành công','success');
          this.router.navigate(['/dashboard']);
        });
      } else {
        swal('', result.message, 'error');
      }
    }).catch(function(error){
      swal('', 'Đăng nhập không thành công.', 'error');
    });
  }
}
