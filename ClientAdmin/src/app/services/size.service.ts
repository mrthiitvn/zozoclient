import { ApiResult } from '../model/api.result.model';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Size } from '../model/size.model';
import { environment } from '../../environments/environment';

@Injectable()
export class SizeService {

    //public _apiService: ApiService;
    constructor(public apiService: ApiService){
        //this._apiService = apiService;
     };

     getSize() : Promise<ApiResult>{
         const url = `${environment.baseApiUrl}/Size/GetAll`;
         return this.apiService.httpGet(url, false);
     }; 

     getSizeById(id) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/Size/GetById?Id=${id}`;
        return this.apiService.httpGet(url, false);
    }; 

    createSize(model) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/Size/Create`;
        return this.apiService.httpPost(url, model, false);
    }; 

    updateSize(model) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/Size/Update`;
        return this.apiService.httpPut(url, model, false);
    }; 

     deleteSize(object) : Promise<ApiResult> {
         const url = `${environment.baseApiUrl}/Size/Delete/${object.Id}`;
         return this.apiService.httpPut(url, object);
     };


     changeSize(object) : Promise<ApiResult> {
        const url = `${environment.baseApiUrl}/Size/ChangeStatus/${object.Id}`;
        return this.apiService.httpPut(url, object);
    };
}
