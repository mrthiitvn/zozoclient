import { ApiResult } from '../model/api.result.model';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Album } from '../model/album.model';
import { environment } from '../../environments/environment';

@Injectable()
export class AlbumService {

    //public _apiService: ApiService;
    constructor(public apiService: ApiService){
        //this._apiService = apiService;
     };

     getAlbum() : Promise<ApiResult>{
         const url = `${environment.baseApiUrl}/Album/GetAll`;
         return this.apiService.httpGet(url, false);
     }; 

     getAlbumById(id) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/Album/GetById?Id=${id}`;
        return this.apiService.httpGet(url, false);
    }; 

    createAlbum(model) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/Album/Create`;
        return this.apiService.httpPost(url, model, false);
    }; 

    updateAlbum(model) : Promise<ApiResult>{
        const url = `${environment.baseApiUrl}/Album/Update`;
        return this.apiService.httpPut(url, model, false);
    }; 

     deleteAlbum(object) : Promise<ApiResult> {
         const url = `${environment.baseApiUrl}/Album/Delete/${object.Id}`;
         return this.apiService.httpPut(url, object);
     };


     changeAlbum(object) : Promise<ApiResult> {
        const url = `${environment.baseApiUrl}/Album/ChangeStatus/${object.Id}`;
        return this.apiService.httpPut(url, object);
    };
}
