import { ApiResult } from '../model/api.result.model';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { UserModel } from '../model/user.model';
import { environment } from '../../environments/environment';

@Injectable()
export class LoginService {

    constructor(private _apiService: ApiService) {

    }
    Login(userName, passWord): Promise<ApiResult> {
        const url = `${environment.baseApiUrl}/Auth/Login`;
        return this._apiService.httpPost(url, { userName, passWord }, false);
    }

    GetMe(id: string): Promise<ApiResult> {
        let url = `${environment.baseApiUrl}/Auth/Me?userId=${id}`;
        return this._apiService.httpGet(url, true);
    }
}