import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from "ngx-bootstrap";
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module


import { routing } from './app.routing';
//import component
import { ViewRouteComponent } from './components/share/view.route.compnent';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { UserComponent } from './components/user/user.component';
import { UserDetailComponent } from './components/user/user.detail.component';
import { OrderComponent } from './components/order/order.component';
import { HeaderComponent } from './components/share/header/header.component';
import { FooterComponent } from './components/share/footer/footer.component';
import { SidebarComponent } from './components/share/sidebar/sidebar.component';
import { ShopComponent } from './components/shop/shop.component';
import { LoginComponent } from './components/login/login.component';
import { ConfigTypeComponent } from './components/config-type/config-type.component';
import { ConfigTypeDetailComponent } from './components/config-type/config-type.detail.component';
import { ConfigComponent } from './components/config/config.component';
import { ConfigDetailComponent } from './components/config/config.detail.component';
import { PhotoTypeComponent } from './components/photo-type/photo-type.component';
import { PhotoTypeDetailComponent } from './components/photo-type/photo-type.detail.component';
import { ImageUploaderComponent } from './components/common/image-uploader/image-uploader.component';
import { RoleComponent } from './components/role/role.component';

//import service
import { ApiService } from './services/api.service';
import { AuthenticateService } from './services/authenticate.service';
import { LoginService } from './services/login.service';
import { SocketService } from './services/socket.service';
import { UserService } from './services/user.service';
import { ConfigTypeService } from './services/config.type.service';
import { ConfigService } from './services/config.service';
import { PhotoTypeService } from './services/photo.type.service';
import { OssImageService } from './services/common/oss-image.service';
import { RoleService } from './services/role.service';
//import utils
import { Utils } from './utils/utils';


//import pipe
import { FilterPipe } from './filter.module';

//import filter
import { Ng2SearchPipeModule } from 'ng2-search-filter'; //importing the module

//import order
import { Ng2OrderModule } from 'ng2-order-pipe'; //importing the module

//Import toast module
import {ToastModule} from 'ng2-toastr/ng2-toastr';
//import { ConfigComponent } from './components/config/config.component';

@NgModule({
  declarations: [
    AppComponent,
    ViewRouteComponent,
    DashboardComponent,
    UserComponent,
    UserDetailComponent,
    OrderComponent,
    HeaderComponent,
    FooterComponent,
    SidebarComponent,
    ShopComponent,
    LoginComponent,
    ConfigTypeComponent,
    ConfigTypeDetailComponent,
    FilterPipe,
    ConfigComponent,
    ConfigDetailComponent,
    PhotoTypeComponent,
    PhotoTypeDetailComponent,
    ImageUploaderComponent,
    RoleComponent
  ],
  imports: [
    BrowserModule, BrowserAnimationsModule, RouterModule, routing, FormsModule, ReactiveFormsModule, ModalModule.forRoot(),
    HttpModule, NgxPaginationModule, Ng2SearchPipeModule, Ng2OrderModule, ToastModule.forRoot()
  ],
  providers: [ApiService, AuthenticateService, LoginService, SocketService, UserService, ConfigTypeService, ConfigService, PhotoTypeService
  , OssImageService, RoleService, Utils
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
