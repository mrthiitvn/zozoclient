export class Scope {
    constructor() {};
    public id: number;
    public name: string;
    public status: number;
}