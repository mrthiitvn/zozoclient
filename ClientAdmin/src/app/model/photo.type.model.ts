export class PhotoType {
    constructor() {};
    public id: number;
    public name: string;
    public shortName: string;
    public width: number;
    public height: number;
    public status: number;
}