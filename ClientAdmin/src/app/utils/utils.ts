import { Injectable } from '@angular/core';

@Injectable()
export class Utils {
    public clone(obj): any {
        if (null == obj || "object" != typeof obj) return obj;
        var copy = obj.constructor();
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = obj[attr];
        }
        return copy;
    }

    public validEmail(email): boolean {
        var reg = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,8}$/;
        return reg.test(email);
    }

    public buildParams(dataUrl) {
        let ret = [];
        for (let d in dataUrl) {
            if (dataUrl[d]) {
                ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(dataUrl[d]));
            }
        }
        return ret.join('&');
    };
    
    public convertTimeFormat(time_format): string{
        if(time_format == '12h'){
            return 'A';
        }else{
            return '';
        }
    };
}