import { Component, OnInit} from '@angular/core';
import { Router} from '@angular/router';
import { SessionData } from './model/session.data.model';
import { AuthenticateService } from './services/authenticate.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent implements OnInit{
  public sessionData: SessionData;
  constructor(
    private authenticateService: AuthenticateService,
    private router: Router
  ) { 
    this.authenticateService.getSessionData().subscribe(o => {
      this.sessionData = o;
    });
  }

  ngOnInit() {debugger;
    if (this.sessionData) {
      this.router.navigate(['/dashboard']);
    } 
    else {
      this.router.navigate(['/login']);
    }
  }
}
